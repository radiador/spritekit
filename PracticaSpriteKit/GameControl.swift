//
//  GameControl.swift
//  PracticaSpriteKit
//
//  Created by formador on 6/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation
import SpriteKit

protocol GameControlNodeDelegate: class {
    
    func controlTouched(gameControlNode: GameControlNode)
}

class GameControlNode: SKSpriteNode {
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture,color: color, size: size)
        
        isUserInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var delegate: GameControlNodeDelegate?
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        delegate?.controlTouched(gameControlNode: self)
    }
}
