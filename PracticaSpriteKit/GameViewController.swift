//
//  GameViewController.swift
//  PracticaSpriteKit
//
//  Created by formador on 6/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

struct PhysicsCategory {
    static let none      : UInt32 = 0
    static let all       : UInt32 = UInt32.max
    static let enemyShip   : UInt32 = 0b1       // 1
    static let projectile: UInt32 = 0b10      // 2
    static let ship: UInt32 = 0b11      // 2
}

class GameViewController: UIViewController {
    
    //Contruimos la escena
    let scene = SKScene()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpScene()
        
        setupShip()
        
        addEnemiesFile()
        
        setupButtons()
    }
    
    //Ai
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let touch = touches.first else {
            return
        }
        
        let location = touch.location(in: view)
        
        if location.y < (view.bounds.size.height - 150)  {
            
            shot()
        }
    }
    
    private func shot() {
        
        //Recuperando el nodo Nave de la lista de nodos de la scena
        guard let ship = scene.childNode(withName: "ship") else { return }
        
        let bullet = SKSpriteNode(imageNamed: "bullet.png")
        
        bullet.position = ship.position
        bullet.size = CGSize(width: 20, height: 30)
        
        bullet.physicsBody = SKPhysicsBody(rectangleOf: bullet.size)
        bullet.physicsBody?.isDynamic = true
        bullet.physicsBody?.categoryBitMask = PhysicsCategory.projectile
        bullet.physicsBody?.contactTestBitMask = PhysicsCategory.enemyShip
        
        bullet.lightingBitMask = PhysicsCategory.all
        bullet.shadowCastBitMask = PhysicsCategory.all

        bullet.run(SKAction.sequence([SKAction.move(by: CGVector(dx: 0, dy: scene.size.height), duration: 5), SKAction.removeFromParent()]))
        
        scene.addChild(bullet)
    }
    
    //Configura la escena y se la añade a la scena
    private func setUpScene() {
        
        if let view = self.view as? SKView {
            // Set the scale mode to scale to fit the view
            scene.scaleMode = .aspectFill
            
            scene.size = view.bounds.size
            
            //Establecemos la fisica general de la scena
            scene.physicsWorld.gravity = .zero
            scene.physicsWorld.contactDelegate = self
            
            // Present the scene
            view.presentScene(scene)
            
            view.showsFPS = true
            view.showsNodeCount = true
            
            view.scene?.backgroundColor = UIColor.blue
            
            let fireOne = SKEmitterNode(fileNamed: "fireEmitter.sks")
            
            fireOne?.position = CGPoint(x: 150, y: 300)
            
            scene.addChild(fireOne!)
            
            
            let lightFire = SKLightNode()
        
            lightFire.position = .zero
        
            lightFire.lightColor = UIColor.yellow
            lightFire.categoryBitMask = PhysicsCategory.all
            lightFire.falloff = 1
            
            fireOne?.addChild(lightFire)
        }
    }
    
    //Creamos los dos "mandos" para mover derecha e izquierda
    private func setupButtons() {
        
        let rightButton = GameControlNode(color: .lightGray, size: CGSize(width: (scene.size.width / 2) - 10, height: 70))
        
        let leftButton = GameControlNode(color: .lightGray, size: CGSize(width: (scene.size.width / 2) - 10, height: 70))
        
        rightButton.name = "right"
        leftButton.name = "left"

        rightButton.position = CGPoint(x: ((scene.size.width / 4) * 3) + 5, y: 80)
        leftButton.position = CGPoint(x: (scene.size.width / 4) + 5, y: 80)
        
        rightButton.delegate = self
        leftButton.delegate = self

        scene.addChild(rightButton)
        scene.addChild(leftButton)
    }
    
    //Configura el nodo nave y se lo añade a la scena
    private func setupShip() {
        
        let ship = SKSpriteNode(color: .red, size: CGSize(width: 20, height: 27))
        
        ship.name = "ship"
        ship.position = CGPoint(x: scene.size.width / 2, y: 150)
        
        ship.physicsBody = SKPhysicsBody(rectangleOf: ship.size)
        ship.physicsBody?.isDynamic = true
        ship.physicsBody?.categoryBitMask = PhysicsCategory.ship
        ship.physicsBody?.contactTestBitMask = PhysicsCategory.enemyShip
        
        ship.lightingBitMask = PhysicsCategory.all
ship.shadowCastBitMask = PhysicsCategory.all
        
        scene.addChild(ship)
    }
    
    //Crea un nodo de nave enemiga
    private func addEnemyShip(position: CGPoint) -> SKNode {
        
        let enemyShip = SKSpriteNode(color: .brown, size: CGSize(width: 18, height: 22))
        
        enemyShip.position = position
        
        enemyShip.physicsBody = SKPhysicsBody(rectangleOf: enemyShip.size)
        enemyShip.physicsBody?.isDynamic = true
        enemyShip.physicsBody?.categoryBitMask = PhysicsCategory.enemyShip
        enemyShip.physicsBody?.contactTestBitMask = PhysicsCategory.ship | PhysicsCategory.projectile
        
        enemyShip.lightingBitMask = PhysicsCategory.all
        enemyShip.shadowCastBitMask = PhysicsCategory.all

        return enemyShip
    }
    
    //Crea una fila de naves enemigas
    private func addEnemiesFile() {
        
        let fileEnemiesNode = SKNode()
        
        fileEnemiesNode.addChild(addEnemyShip(position: CGPoint(x: 50, y: 0)))
        fileEnemiesNode.addChild(addEnemyShip(position: CGPoint(x: 100, y: 0)))
        fileEnemiesNode.addChild(addEnemyShip(position: CGPoint(x: 150, y: 0)))
        fileEnemiesNode.addChild(addEnemyShip(position: CGPoint(x: 200, y: 0)))
        
        
        fileEnemiesNode.position = CGPoint(x: 0, y: scene.size.height - 50)
        
        let moveToRightAction = SKAction.move(by: CGVector(dx: 200, dy: 0), duration: 3)
        let downFileAction = SKAction.move(by: CGVector(dx: 0, dy: -40), duration: 1)
        let moveToLeftAction = SKAction.move(by: CGVector(dx: -200, dy: 0), duration: 3)

        let squenceAction = SKAction.sequence([moveToRightAction, downFileAction, moveToLeftAction, downFileAction])
        
        fileEnemiesNode.run(SKAction.repeat(squenceAction, count: 10))
        
        scene.addChild(fileEnemiesNode)
    }
}

extension GameViewController: GameControlNodeDelegate {
    
    func controlTouched(gameControlNode: GameControlNode) {
        
        guard let ship = scene.childNode(withName: "ship") else { return }
        
        ship.removeAllActions()

        if gameControlNode.name == "right" {
            
            ship.run(SKAction.move(by: CGVector(dx: 10, dy: 0), duration: 0.5))
        } else {
            
            ship.run(SKAction.move(by: CGVector(dx: -10, dy: 0), duration: 0.5))
        }
    }
}

extension GameViewController: SKPhysicsContactDelegate {
    
    func didBegin(_ contact: SKPhysicsContact) {
        // 1
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        // 2
        if (firstBody.categoryBitMask == PhysicsCategory.enemyShip ) {
            
            if (secondBody.categoryBitMask == PhysicsCategory.ship) {
                
                print("Perdiste")
                if let view = self.view as? SKView {
                    
                    view.isPaused = true
                }
            } else if (secondBody.categoryBitMask == PhysicsCategory.projectile ) {
                
                print("nave destruida")

                firstBody.node?.removeFromParent()
            }
        }
    }
}
